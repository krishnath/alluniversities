# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

import os
import json
import applications.tweeket.modules.configuration as configuration
import re
import time
import ast

#nltk imports
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords
from nltk.probability import FreqDist
from __builtin__ import dict

# encoding=utf8  
import sys  
from string import lower
from time import sleep
reload(sys)  
sys.setdefaultencoding('utf8')

def search():
    
    return dict(message="hello")


def index():
    """
	This function shows the entry page for the tweeket 
	app!!!
    """
    response.flash = T("Welcome to tweeket!!!")
    #===========================================================================
    # configParser = ConfigParser.RawConfigParser()
    # configFilePath = r'config.txt'
    # configParser.read(configFilePath)
    # jsonLocation = configParser.get('files','jsonLocation')
    #===========================================================================
    jsonLocation = configuration.configuration.jsonLocation
    os.chdir(jsonLocation)
    filelist= os.listdir(jsonLocation)
    
    arrjsons = []
    
    for i in filelist:
        
        if not ".json" in i:
            
            filelist.remove(i)
        else:
            with open(i) as file:
                file.name
                data = json.load(file)
                arrjsons.append(data)
    arrTweets = []
    for i in arrjsons:
        tempmap = {}
        tempmap["created_at"] = i["created_at"]
        tempmap["text"]= i["text"]
        tempmap["user"] = i["user"]
        tempmap["source"] = i["source"]
        tempmap["tweeket"] = {}
        tempmap["tweeket"]["tweet"] = i["text"]
        tempmap["tweeket"]["type"] = "tweeket"
        tempmap["tweeket"]["subject"] = "tweet created at " + i["created_at"]
        tempmap["tweeket"]["status"] = "new"
        tempmap["tweeket"]["created_at"] = i["created_at"]
        tempmap["tweeket"]["user_screenname"] = i["user"]["screen_name"]
        tempmap["jiratweeket"] = {}
        tempmap["jiratweeket"]["fields"] = {}
        tempmap["jiratweeket"]["fields"]["project"] = {}
        tempmap["jiratweeket"]["fields"]["project"]["key"] = "TEST"
        tempmap["jiratweeket"]["fields"]["summary"] = "Creating an issue for tweet created at " + i["created_at"]
        tempmap["jiratweeket"]["fields"]["description"] = "A tweet created by "+i["user"]["screen_name"]+" may need some attention"
        tempmap["jiratweeket"]["fields"]["issuetype"] = {}
        tempmap["jiratweeket"]["fields"]["issuetype"]["name"] = "Bug"
        tempmap["jiratweeket"]["fields"]["customfield_11050"] = tempmap["tweeket"]
        tempmap["bugztweeket"] ={}
        tempmap["bugztweeket"]["product"] = "TESTPRODUCT"
        tempmap["bugztweeket"]["component"] = "TESTCOMPONENT"
        tempmap["bugztweeket"]["version"] = "1.0"
        tempmap["bugztweeket"]["summary"] = tempmap["tweeket"]

        #Freshdesk json
        tempmap["freshdeskjson"] = {}
        tempmap["freshdeskjson"]["helpdesk_ticket"] = {}
        tempmap["freshdeskjson"]["helpdesk_ticket"]["description"] = tempmap["tweeket"]
        tempmap["freshdeskjson"]["helpdesk_ticket"]["subject"] = "Creating an issue for tweet created at " + i["created_at"]
        tempmap["freshdeskjson"]["helpdesk_ticket"]["email"] = "sample@sample.com"
        tempmap["freshdeskjson"]["helpdesk_ticket"]["priority"] = 1
        tempmap["freshdeskjson"]["helpdesk_ticket"]["status"] = 2
        
        


        arrTweets.append(tempmap)
        
    return dict(arrTweets=arrTweets)

def caculatetweetquality():
    
    actualtxttweet = request.vars['txttweet']

    actualtxttweet = actualtxttweet.lower() 
    txttweet = actualtxttweet.split()

    englishstopwords = stopwords.words('english')
    stopwordscount = 0
    
    for i in txttweet:
        if i in englishstopwords:
            stopwordscount += 1
    qualitypercent = (stopwordscount/len(txttweet))
    qualitylevel = 0
    if(qualitypercent > 0.6):
        qualitylevel = -1
    elif(qualitypercent > 0.4):
        qualitylevel = 0
    else:
        qualitylevel = 1

    return dict(stopwordscount=stopwordscount,qualitylevel=qualitylevel)
    
def calculatesentiment():
    actualtxttweet = request.vars['txttweet'] # txttweet is a string here
    
    actualtxttweet = actualtxttweet.lower() 
    txttweet = actualtxttweet.split() # now txttweet is  being used as an array
    
#     txttweet = "This is a really good one"
    #Calculating good and bad words
    #TODO: this part is to be replaced with static fetch or sqlite file or a plain text in future
    #TODO: this part is also to be extended with an extended set of good or bad words
    goodsynonyms = findallSynonyms("good")
    goodsynonyms.extend(findallSynonyms("positive"))
    goodsynonyms.extend(findallSynonyms("happy"))
    

    badsynonyms = findallSynonyms("bad")
    badsynonyms.extend(findallSynonyms("negative"))
    badsynonyms.extend(findallSynonyms("sad"))
    
    
    
#     return dict(goodsynonyms=goodsynonyms,badsynonyms=badsynonyms)
    #finding which count is large ...good or bad words
    badcount = 0
    goodcount = 0
    freqdist = FreqDist(txttweet)
#     return dict(freqdist=freqdist)
    for word in goodsynonyms:
        goodcount = goodcount + freqdist[word.lower()]
    for word in badsynonyms:
        badcount = badcount + freqdist[word.lower()]
    
#     for word in txttweet:
#         
#         if word.lower() in goodsynonyms:
#             goodcount=goodcount+1;
#         if word.lower() in badsynonyms:
#             badcount=badcount+1;
    
    
    
    #Reading Oxymorons processing with good bad counting
    oxymoronfileloc = configuration.configuration.oxymoronfile
    
    oxymoronfile = open(oxymoronfileloc)
    strlines = oxymoronfile.readlines()
    somethingtolook = False
    
    templist = []
    tempoxymoron = ""
    goodorbad= ""
    strength = 0
    
    

    for line in strlines:
        line = line.strip('\n')
        
        templist = ast.literal_eval(line)
        
    
        tempoxymoron = templist[0]
        goodorbad = templist[1]
        strength = templist[2]
    
        if tempoxymoron in actualtxttweet:
            somethingtolook = True

            if (lower(goodorbad) == "good"):
                goodcount += int(strength)
            elif (lower(goodorbad) == "bad"):
                badcount += int(strength)
            
    #Finally calculate the difference and also the sentiment analysis 
    sentimentdifference = goodcount - badcount
    if sentimentdifference > 0:
        sentiment = "good"
    elif sentimentdifference < 0:
        sentiment = "bad"
    else:
        sentiment = "neutral"
        
    
    return dict(sentiment=sentiment,sentimentdifference=sentimentdifference,somethingtolook=somethingtolook)

def calculatesentiment1():
    return dict(this="asdfasdf")            

def findallSynonyms(text):
    synsets = wn.synsets(text)
    synonymlist = []
    for synset in synsets:
        synonymlist.extend(synset.lemma_names())
    return list(set(synonymlist))
    
    
    
    

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


