from google.appengine.ext import db

class uni_usa(db.Model):
        name = db.StringProperty(multiline=True)
        website = db.StringProperty(multiline=True)

def getuniversities(start,end):
	q = db.GqlQuery("select * from uni_usa order by name").run(offset=start, limit=end-start)
	return q

def getuniversitieslike(strName,start,end):
	if ((strName) and strName != ""):
		q = db.GqlQuery("select * from uni_usa where name >=:1 and name <:2", strName, strName + u"\ufffd").run(offset=start,limit=end-start)
		return q	
	else:		
		q = db.GqlQuery("select * from uni_usa order by name").run(offset=start,limit=end-start)
		return q
