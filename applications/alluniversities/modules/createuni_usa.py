import os
from time import sleep
from Model import uni_usa

names = []
websites = []

with open('universitiesnames.txt') as f:
    content = f.readlines()

for i in content:
	if i.strip() == "":
		names.append(i.strip())
		print "Line is empty"
	else:
		names.append(i.decode('utf-8').strip()) 
		print i

with open('universitieswebsites.txt') as f:
	content = f.readlines()

for i in content:
	if i.strip() == "":
		print "Line is empty"
		websites.append(i)
	else:
		print i
		websites.append(i.decode('utf-8').replace('\n', ' ').replace('\r', '').strip())


for i in range(len(names)):
	print("website is"+websites[i])
	uni = uni_usa(name=names[i],website=websites[i])
	uni.put()
