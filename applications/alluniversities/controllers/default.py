# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#Below import is needed for performing a GPQLQuery
from google.appengine.ext import db as dstore
import dbinterface

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    grid = SQLFORM.grid(db.uni_usa,fields=[db.uni_usa.name],links=[dict(header='webpage',body= lambda r:A('webpage',_href='http://%s'%r.website,_target='new'))],csv=False,maxtextlength=50,searchable=False,details=False)
    response.flash = T("Hello Universities of USA!!!")
    return dict(grid=grid)


def showuniversitiesstartwith():
	sum=0
	if ((request.vars.startswith == "") or not(request.vars.startswith)):
		redirect(URL(vars={'page':1,'startswith':'A'}))
	if ((not request.vars.page) or (int(request.vars.page) < 1) ):
		redirect(URL(vars={'page':1,'startswith':request.vars.startswith}))
	else:
		page = int(request.vars.page)
		start = (page-1)*10
		end = page*10
		strstring = request.vars.startswith
		results = list(dbinterface.getuniversitieslike(strstring,start,end))
		for i in results:
			sum +=1
		if(sum  > 0):
			return dict(unis=results,startswith=strstring)
		if(sum == 0 and page > 1):
			redirect(URL(vars={'page':1,'startswith':strstring}))

def showuniversities():
	"""
	list all universities with pagination
	"""
	if (not request.vars.page or int(request.vars.page) < 1):
		redirect(URL(vars={'page':1}))
	else:
		page = int(request.vars.page)
		
	start = (page-1)*10
	end = page*10
	#unis = db(db.uni_usa).select(orderby=db.uni_usa.name, limitby=(start,end))
	unis=dbinterface.getuniversities(start,end)
	return dict(unis=unis)


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


