# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
# # This is a sample controller
# # - index is the default action of any application
# # - user is required for authentication and authorization
# # - download is for downloading files uploaded in the db (does streaming)
#########################################################################
from collections import Counter
import re
from pymongo import MongoClient
from audioop import reverse

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))

def findcountry(ip):
    incomingiptuple = tuple([int(x) for x in ip.split(".")])   
     # Connecting to the client
    client = MongoClient()
    db = client['test']
    regex = re.compile("^" + str(incomingiptuple[0]))
    for mapping in db.ipmapping.find({"$or":[{"from":regex}, {"to":regex}]}):
        if (any(c.isalpha() for c in mapping["from"]) or any(c.isalpha() for c in mapping["to"])):
            continue
        fromtuple = tuple([int(x) for x in mapping["from"].split(".")])
        totuple = tuple([int(x) for x in mapping["to"].split(".")])
        if(fromtuple <= incomingiptuple and incomingiptuple <= totuple):
            return mapping["country"]
    return None
    
    
def dashboardCountry():
    ipstats = getHostStats()["ipstats"]
    dashboardstats = []
    for key in ipstats.keys():
        if (not any(c.isalpha() for c in key)):
            dashboardstats.append(findcountry(key))
    return dict(countrystats=Counter(dashboardstats))    
        
def getHostStats():
    """ calculate and return only the ip statistics"""
    filepath = "/Users/krish/Documents/angularjs/logan/apache-samples/access_log/access_log"
    filecontents = open(filepath, 'r')
    res = []
    for line in filecontents.readlines():
        """inserting only the ips numbers"""
        res.append((line.split())[0])
    """Now calculate the statistics"""
    return dict(ipstats=Counter(res))

def gethttpStats():
    """ calculate and return only the ip statistics"""
    filepath = "/Users/krish/Documents/angularjs/logan/apache-samples/access_log/access_log"
    filecontents = open(filepath, 'r')
    res = []
    for line in filecontents.readlines():
        """inserting only the ips numbers"""
        res.append((line.split())[-2])
    """Now calculate the statistics"""
    return dict(httpstats=Counter(res))

def dashboardResource():
    resourceStats = getresourceStats()["resourcestats"]
    dashboardstats = {}
    i=0
    for key, value in sorted(resourceStats.iteritems(), key=lambda (k, v): (v, k),reverse=True):
        dashboardstats[key] = value
        i = i+1
        if(i==10):
            break
            
    return dict(stats=dashboardstats)
 

def getresourceStats():
    """ calculate and return only the ip statistics"""
    filepath = "/Users/krish/Documents/angularjs/logan/apache-samples/access_log/access_log"
    filecontents = open(filepath, 'r')
    res = []
    for line in filecontents.readlines():
        """inserting only the ips numbers"""
        res.append(line[line.index('"') + 1:line.index('"', line.index('"') + 1)])
    """Now calculate the statistics"""
    return dict(resourcestats=Counter(res))

        
    
        
    
    return dict(message="done")
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


