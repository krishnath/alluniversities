import os
import sys
import twitter
import networkx as nx
from getretweetgraph import create_rt_graph
from getoauth import *

def write_dot_output(g, out_file):
	try:
		nx.drawing.write_dot(g, out_file)
		print >> sys.stderr, 'Data file written to', out_file
	except ImportError,e :
		dot = ['"%s" -> "%s" [tweet_id=%s]' % (n1,n2,g[n1][n2]['tweet_id']) for (n1,n2) in g.edges()]
		f = open(out_file, 'w')
		f.write('''strict digraph{%s}''' % (';\n'.join(dot), ))
		f.close()
		print >> sys.stderr, 'Data file written to: %s' % f.name

if __name__ == "__main__":
	Q = ' '.join(sys.argv[1])
	OUT = 'twitter_retweet_graph'
	MAX_PAGES = 10
	twitter_search = main()
	search_results = twitter_search.search.tweets(q=Q, count=10)
	all_tweets = [tweet for tweet in search_results['statuses']]
	g = create_rt_graph(all_tweets)
	if not os.path.isdir('out'):
		os.mkdir('out')
	f = os.path.join(os.getcwd(), 'out', OUT)
	write_dot_output(g,f)
	print >> sys.stderr , 'Try this on the DOT output: $ dot -Tpng -0%s %s.dot' % (f,f,)
