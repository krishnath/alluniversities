import json
import twitter_text

def get_entities(tweet):
	extractor = twitter_text.Extractor(tweet['text'])
	entities = {}
	entities['user_mentions'] = []
	for um in extractor.extract_mentioned_screen_names_with_indices():
		entities['user_mentions'].append(um)
	entities['hashtags'] = []
	for ht in extractor.extract_hashtags_with_indices():
		ht['text'] = ht['hashtag']	
		del ht['hashtag']
		entities['hashtags'].append(ht)
	entities['url'] = []
	for url in extractor.extract_urls_with_indices():
		entities['urls'].append(url)
	return entities


if __name__ == "__main__":
	tweets 
