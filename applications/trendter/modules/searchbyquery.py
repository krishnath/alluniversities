#Use the search query instead of rpp and max
import sys
import json
import twitter
from getoauth import *

Q=' '.join(sys.argv[1])

MAX_PAGES=15
RESULTS_PER_PAGE=100
twitter_search = main()
search_results = []
for page in range(1,MAX_PAGES+1):
	search_results += twitter_search.search(q=Q, rpp=RESULTS_PER_PAGE, page=page)['results']
print json.dumps(search_results, indent =1)
