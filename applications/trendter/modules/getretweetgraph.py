import sys
import json
import twitter
import networkx as nx
from retweetorigin import get_rt_origins
from getoauth import main

def create_rt_graph(tweets):
	g = nx.DiGraph()
	for tweet in tweets:
		rt_origins = get_rt_origins(tweet)
		if not rt_origins:
			continue
		for rt_origin in rt_origins:
			g.add_edge(rt_origin.encode('ascii','ignore'), tweet['user']['name'].encode('ascii','ignore'), {'tweet_id':tweet['id']})
	return g
	
if __name__ == "__main__":
	twitter_search = main()
	search_results = twitter_search.search.tweets(q="India",count=10)
	print "single result is ", search_results['statuses'][1]
	all_tweets = [tweet for tweet in search_results['statuses']]
	g = create_rt_graph(all_tweets)
	print >> sys.stderr, "Number nodes:", g.number_of_nodes()
	print >> sys.stderr, "Num edges:", g.number_of_edges()
	print >> sys.stderr, "Num connected components:", len(list(nx.connected_components(g.to_undirected())))
	print >> sys.stderr, "Node degrees:", sorted(nx.degree(g))
		
