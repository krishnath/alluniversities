import json
import twitter_text
import getoauth
from time import sleep
from getoauth import *

def get_entities(tweet):
	try:
		print("inside try of get_entities")
		extractor = twitter_text.Extractor(tweet['text'])
		entities={}
		entities['user_mentions'] = []
		for um in extractor.extract_mentioned_screen_names_with_indices():
			entities['user_mentions'].append(um)
		entities['hashtags'] = []
		for ht in extractor.extract_hashtags_with_indices():
			ht['text'] =  ht['hashtag']
			del ht['hashtag']
			entities['hashtag'].append(ht)
		entities['urls'] = []
		for url in extractor.extract_urls_with_indices():
			entities['urls'].append(url)
		print "inside get_entities",entities
		return entities
	except Exception,e:
		print("Error is ",e)

if __name__ == "__main__":
	t=main()
	tweets =t.search.tweets(q="India",count=10)
	print "tweets is", tweets['statuses']
	sleep(3)
	for tweet in tweets['statuses']:
		tweet['entities'] = get_entities(tweet)
	#print json.dumps(tweets, indent=1)
	print("-------------------------")
	for tweet in tweets['statuses']:
		print tweet['entities']
		sleep(2)
