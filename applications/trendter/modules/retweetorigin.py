import re
from getoauth import *
from time import sleep
def get_rt_origins(tweet):
	rt_patterns = re.compile(r"(RT|via)((?:\b\W*@\w+)+)", re.IGNORECASE)
	rt_origins = []
	
	if tweet.has_key('retweet_count'):
		if tweet['retweet_count'] > 0:
			rt_origins += [ tweet['user']['name'].lower()]
	
	try:
		rt_origins += [mention.strip() for mention in rt_patterns.findall(tweet['text'])[0][1].split()]
	except IndexError, e:
		pass
	#to remove duplicates
	return list(set([rto.strip("@").lower() for rto in rt_origins]))
	
if __name__ == "__main__":
	t =  main()
        tweets =t.search.tweets(q="India",count=10)
	print "tweets is", tweets['statuses']
	for tweet in tweets['statuses']:
		print "tweet is ", tweet
		sleep(2)
		print "oringin is   ",get_rt_origins(tweet) 
		sleep(2)
