import sys
import time
from urllib2 import URLError
import twitter
from getoauth import *

def make_twitter_request(t, twitterFunction, max_errors=3, *args, **kwArgs):
	def handle_http_error(e,t, wait_period=2):
		if wait_period > 3600:
			print >> sys.stderr, 'Too many retries. Quitting.'
			raise e
		if e.e.code == 401:
			print >> sys.stderr, 'Encountered 401 Error (Not Authorized)'
			return None
		elif e.e.code in (502, 503):
			print >> sys.stderr, 'Encountered %i Error. Will retry in %i seconds' % (e.e.code, wait_period)
			time.sleep(wait_period)
			wait_period *= 1.5 
			return wait_period
		elif t.account.rate_limit_status()['remaining_hits'] == 0:
			status = t.account.rate_limit_status()
			now = time.time() #UTC
			when_rate_limit_resets = status['reset_time_in_seconds']
			sleep_time = when_rate_limit_resets - Now
			print >> sys.stderr, 'Rate limit reached: sleeping for %i secs' % (sleep_time, )
			time.sleep(sleep_time)
			return 2
		else:
			raise e
		wait_period = 2
		error_count = 0
		while True:
			try:
				print "before return"
				return twitterFuction(*args, **kwArgs)
				print "after return"
			except twitter.api.TwitterHTTPError, e:
				error_count = 0
				wait_period = handle_http_error(e,t,wait_period)
				if wait_period is None:
					return
			except URLError, e:
				error_count += 1
				print >> sys.stderr, "URLError encountered. Continuing."
				if error_count > max_errors:
					print >> sys.stderr, "Too many consecutive errrors, bailing out.."
					raise

if __name__ == "__main__":
	t = main()
	print(make_twitter_request(t, t.followers.ids, screen_name="SocialWebMining", cursor=-1))
