# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
#list of imports
import json
import os
def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    
    response.flash = T("Welcome to Trendter!!!")
    #jsonWorldTrends= json.load(open("/Users/krish/Documents/pythonworkspace/Recipes-for-Mining-Twitter/jsons/world.json"))
    os.chdir("/Users/krish/Documents/pythonworkspace/Recipes-for-Mining-Twitter/worldjsons")
    filelist= os.listdir("/Users/krish/Documents/pythonworkspace/Recipes-for-Mining-Twitter/worldjsons")
    mapDailyTrends = {}
    #Put file and its contents in a map where filename is the key
    for i in filelist:
	mapDailyTrends[i] = open(i)
    print "mapDailyTrends is"
    DailyList = []
    mapDailyUrlList = {} 
    for i in mapDailyTrends:
	for j in json.load(mapDailyTrends[i])[0]["trends"]:
		DailyList.append(j["url"])
	mapDailyUrlList["i"] = DailyList
	DailyList = []
    
    #urls=[]
    #for i in jsonWorldTrends[0]["trends"]:
    #	urls.append(i["url"])    		
    #return dict(message=T('Welcome to web2py!'),urls=urls)
    return dict(message=T('Welcome to web2py!'),mapDailyUrlList=mapDailyUrlList)


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


